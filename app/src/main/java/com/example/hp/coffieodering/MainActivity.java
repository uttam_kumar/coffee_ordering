package com.example.hp.coffieodering;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    int quantity=0;
    int price=5;
    int total_price=0;
    String whipped;
    String chocolate,tt;
    boolean checked;
    boolean checkedC;
    String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void submitOrder(View view){
        EditText e_text=(EditText) findViewById(R.id.name_text);
        name= e_text.getText().toString();

        CheckBox check_V= (CheckBox) findViewById(R.id.whipped_cream_check);
        checked= check_V.isChecked();
        whipped="Add Whipped Cream ? "+checked;

        if(checked==true)
            price=6;

        CheckBox check_C= (CheckBox) findViewById(R.id.chocolate_check);
        checkedC= check_C.isChecked();
        chocolate="Add Chocolate ? "+checkedC;

        if(checkedC==true)
            price=7;

        if(checked==true && checkedC==true){
            price=8;
        }

        total_price=price*quantity;
        displaysummary(quantity,name, whipped, chocolate, total_price);




    }

    public void submitOrderIncrement(View view){
        if(quantity!=100)
            quantity=quantity+1;
        display(quantity);
        if(quantity==100)
            Toast.makeText(this, "You can not have greater than 100 cup of coffee", Toast.LENGTH_SHORT).show();

    }

    public void submitOrderDecrement(View view){
        if(quantity>1)
            quantity=quantity-1;
        display(quantity);
        if(quantity==1)
            Toast.makeText(this, "You can not have less than 1 cup of coffee", Toast.LENGTH_SHORT).show();

    }
    private void display(int number){
        TextView quantityTextView = findViewById(R.id.quantity_text_view);
        quantityTextView.setText(""+number);
    }

    private void displaysummary(int number,String sname, String swhipped, String schocolate, int total){
        TextView txt_v=(TextView) findViewById(R.id.summary);
        txt_v.setText("ORDER SUMMARY\n\nName : "+sname+"\nQuantity : "+number+"\n"+swhipped+"\n"+schocolate+"\nTotal : "+ NumberFormat.getCurrencyInstance().format(total)+"\nThank you !!!");
    }
    public void sendOrder(View view){
        tt="Name : "+name+"\nQuantity : "+quantity+"\n"+whipped+"\n"+chocolate+"\nTotal : "+ NumberFormat.getCurrencyInstance().format(price)+"\nThank you !!!";
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Coffie order for-->"+name);
        intent.putExtra(Intent.EXTRA_TEXT,tt );
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


}
